alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias less='less -R'
alias ssudo='sudo -sE'

# Don't override by default
alias cp='cp -i'
alias mv='mv -i'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency="$([ $? = 0 ] && echo normal || echo critical)" -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias jadmin='cd $(fd . ~/Documents/Administration/ --type d |fzf)'
alias jdir='cd "$(fd --hidden --type d | fzf)"'
alias v='~/bin/vifmrun'
alias m='neomutt'
alias offlineimap='/usr/bin/python3 -B /usr/bin/offlineimap'
alias sxiv=/usr/bin/nsxiv
alias gdbus=/usr/lib/qt6/bin/qdbus
