# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Disable ctrl+s.
stty -ixon

# Use vi mode
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'
bind -m vi-command 'Control-p: previous-history'
bind -m vi-insert 'Control-p: previous-history'
bind -m vi-command 'Control-n: next-history'
bind -m vi-insert 'Control-n: next-history'

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# History
HISTSIZE=
HISTFILESIZE=
HISTTIMEFORMAT="%d/%m/%y %T "
HISTCONTROL=ignoreboth:erasedups
PROMPT_COMMAND="history -a; history -n"

shopt -s histappend
shopt -s histreedit
shopt -s histverify

# Tab complete options
bind 'TAB':menu-complete
bind "set show-all-if-ambiguous on"
bind "set menu-complete-display-prefix on"

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

export PS1='\[\e[1;31m\][\[\e[32m\]\u@\h \[\e[34m\]\w\[\e[31m\]] \$\[\e[m\] '

# Make less more friendly for non-text input files, see lesspipe(1).
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

export EDITOR='/usr/bin/vim'
export TERMINAL='/usr/bin/foot'
export PAGER='/usr/bin/less'

# gpg and ssh agents
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye &>/dev/null

# FZF
export FZF_DEFAULT_COMMAND='fd -H'
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
eval "$(fzf --bash)"

# Virtualenv wrapper
VIRTUALENV_WRAPPER="/usr/bin/virtualenvwrapper_lazy.sh"
[ -f "$VIRTUALENV_WRAPPER" ] && . "$VIRTUALENV_WRAPPER"

# Colored man
man() {
 env \
  LESS_TERMCAP_mb=$(printf "\e[1;31m") \
  LESS_TERMCAP_md=$(printf "\e[1;31m") \
  LESS_TERMCAP_me=$(printf "\e[0m") \
  LESS_TERMCAP_se=$(printf "\e[0m") \
  LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
  LESS_TERMCAP_ue=$(printf "\e[0m") \
  LESS_TERMCAP_us=$(printf "\e[1;32m") \
   man "$@"
}

# Local bashrc not tracked in git
[ -f ~/.bashrc_local ] && . ~/.bashrc_local
[ -f ~/.bash_aliases ] && . ~/.bash_aliases
[ -f ~/.cargo/env ] && . ~/.cargo/env
[ -f ~/.ghcup/env ] && . ~/.ghcup/env

# ===========
# Extra paths
# ===========

# Go path
export GOPATH=$HOME/.go
export PATH="$PATH:$GOPATH/bin"

# Rust path
export CARGO_PATH=$HOME/.cargo
export PATH="$PATH:$CARGO_PATH/bin"

# NPM path
# Run `npm config set prefix $NPM_PATH`
export NPM_PATH=$HOME/.npm-global
export PATH="$PATH:$NPM_PATH/bin"

# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
export PATH="$PATH:$PNPM_HOME"

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PATH:$PYENV_ROOT/bin"
which pyenv > /dev/null 2>&1 && eval "$(pyenv init -)"

# Ruby path
RUBY_PATH=$HOME/.local/share/gem/ruby
for f in $RUBY_PATH/*/bin; do
    PATH="$PATH:$f"
done

# Tidal cycles path
export PATH="$PATH:$HOME/.vim/pack/plugins/start/vim-tidal/bin"

# Home bin dir
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/bin"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
