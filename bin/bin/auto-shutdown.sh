#!/bin/sh

# Name: auto-shutdown.sh
# Brief: Shutdown a server when it is not in used
# Author: Nicolas Porcel

# 0 = EXIT_SUCCESS
# 1 = EXIT_FAILURE
readonly TRUE=0
readonly FALSE=1

# Default values for global variables
INACTIVITY_TIMEOUT=3600
INTERVAL=60
BTRFS_VOLUMES=
SAMBANETWORK=
STATUS_FILE=/var/run/auto-shutdown
DISABLE_SHUTDOWN_FILES=/var/run/auto-shutdown.disable
LOG_FILE=/var/log/auto-shutdown.log
CONF_FILE=/etc/auto-shutdown.conf
CRON_MODE=false

usage()
{
  echo "Usage: $(basename $0) [-f filename] [-i interval] [-c]"
}


checkScrub()
{
  local volume
  for volume in $BTRFS_VOLUMES ; do
    if (btrfs scrub status $volume | grep running > /dev/null 2>&1) ; then
      return $TRUE
    fi
  done
  return $FALSE
}


checkDisableFiles()
{
  local file
  for file in $DISABLE_SHUTDOWN_FILES ; do
    if [ -f $file ] ; then
      return $TRUE
    fi
  done

  return $FALSE
}


log()
{
  echo $(date): "$@" >> $LOG_FILE
}


isBusy()
{
  log "Checking activity..."
  local network_activity

  # Disable auto-shutdown if this file is present
  if (checkDisableFiles) ; then
    log checkDisableFiles
    return $TRUE
  fi

  # Check active connections
  network_cmd="ss -tu -o state established"
  if [ $($network_cmd | wc -l) -gt 1 ] ; then
    log network
    log "$($network_cmd)"
    return $TRUE
  fi

  # Check connected users
  if [ $(who | wc -l) -gt 0 ] ; then
    log users
    return $TRUE
  fi

  # Check for btrfs scrub
  if (checkScrub) ; then
    log checkScrub
    return $TRUE
  fi

  # Check samba connections
  if [ "$SAMBANETWORK" ] ; then
    if (smbstatus | grep $SAMBANETWORK > /dev/null 2>&1) ; then
      log smbstatus
      return $TRUE
    fi
  fi

  log "No activity"
  return $FALSE
}


checkTimeout()
{
  local last_activity_time
  local inactivity_duration
  local curtime
  local timeout

  curtime=$(date +%s)

  # If the status file does not exist, this means that the timeout was reset.
  # Write the current date to the file to indicate that change.
  if ! [ -f $STATUS_FILE ] ; then
    date +%s > $STATUS_FILE
  fi

  # Get the last activity time stamp
  last_activity_time=$(cat $STATUS_FILE)
  inactivity_duration=$(($curtime - $last_activity_time))

  # Check if the difference between last activity time stamp
  # and current time stamp is greater than the timeout
  test $inactivity_duration -ge $INACTIVITY_TIMEOUT
}


doAutoShutdown()
{
  if (isBusy) ; then
    # The system is busy, record the current time
    date +%s > $STATUS_FILE
  else
    # The system is not busy
    if (checkTimeout) ; then
      # Timeout is reached, do the shutdown
      [ -f $STATUS_FILE ] && rm $STATUS_FILE
      pm-suspend
    fi
  fi
}

while getopts ":f:i:c" opt; do
  case $opt in
    f) CONF_FILE=$OPTARG ;;
    i) INTERVAL=$OPTARG ;;
    c) CRON_MODE=true ;;
    *) usage; exit 1 ;;
  esac
done

# Source conf file if exists
[ -f $CONF_FILE ] && . $CONF_FILE

if $CRON_MODE ; then
  # Cron mode: check idle status and exit
  doAutoShutdown
else
  # Regular mode: check idle status forever and every INTERVAL seconds
  while true
  do
    doAutoShutdown
    sleep $INTERVAL
  done
fi

exit 0
