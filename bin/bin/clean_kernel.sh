#!/bin/bash

VERSION="$1"

echo Removing /boot/*${VERSION}{,.img} /lib/modules/${VERSION}...
rm -i /boot/*${VERSION}{,.img}
rm -rI /lib/modules/${VERSION}
