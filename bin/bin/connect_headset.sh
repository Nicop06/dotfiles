#!/bin/bash

bluetoothctl power on
device=$(bluetoothctl devices | fzf -1 | awk '{print $2;}')
bluetoothctl connect $device
