#!/bin/bash

[ $# -lt 2 ] && exit 1

QUESTION="$1"
COMMAND="$2"

dmenu_cmd=$([ "${XDG_SESSION_TYPE}" = "wayland" ] && echo wmenu || echo dmenu)
ANSWER=$(echo -e 'No\nYes' | $dmenu_cmd -i -p "$QUESTION")

[ "$ANSWER" = "Yes" ] && $COMMAND
