#!/bin/sh

REMOTE_IP=192.168.0.163
SCRIPT_DIR=`dirname $0`
DIR=${1:-sound}

pushd ${SCRIPT_DIR}/${DIR}
rm *.tar.xz
make tarxz-pkg -j9 -l8
TARBALL=$(ls *.tar.xz)
scp ${TARBALL} ${REMOTE_IP}:
ssh ${REMOTE_IP} ./install.sh ${TARBALL}
popd
