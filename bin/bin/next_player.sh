#!/bin/sh
readonly NOTIFY_ID=555
readonly NOTIFY_TIME=2000
readonly ICON="/usr/share/icons/Faba/48x48/apps/multimedia-audio-player.svg"

playerctld daemon 2> /dev/null
playerctld shift

readonly PLAYER="$(playerctl -l | head -n1)"
echo $PLAYER

notify-send "$PLAYER" -i $ICON --replace-id=$NOTIFY_ID -t $NOTIFY_TIME
