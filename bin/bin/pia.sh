#!/bin/sh

CONFIG_PREFIX="$HOME/.config/openvpn/pia"
CONFIG="$1"

if ! [ -f "$CONFIG" ]
then
  CONFIG="$CONFIG_PREFIX/$(cd $CONFIG_PREFIX; ls *.ovpn | fzf -1 -q "$1")"
fi

echo $CONFIG

sudo rm -f /dev/shm/pia
touch /dev/shm/pia
chmod 600 /dev/shm/pia
pass mozilla/firefox/privateinternetaccess.com | grep --color=never login: | sed 's/login: //' > /dev/shm/pia
pass mozilla/firefox/privateinternetaccess.com | head -n1 >> /dev/shm/pia
sudo chown root:root /dev/shm/pia
(sleep 10 && sudo rm -f /dev/shm/pia) &

sudo /opt/piavpn/bin/pia-openvpn --config "$CONFIG" --auth-user-pass /dev/shm/pia
