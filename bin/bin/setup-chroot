#!/bin/sh

start() {
  mount -o bind /dev $1/dev
  mount -o bind /dev/pts $1/dev/pts
  mount -o bind /dev/shm $1/dev/shm
  mount -t proc none $1/proc
  mount -t sysfs none $1/sys
  mount -t tmpfs none $1/tmp
  if [ -n "$XDG_RUNTIME_DIR" ]
  then
    mkdir -p "$1/$XDG_RUNTIME_DIR"
    mount -o bind "$XDG_RUNTIME_DIR" "$1/$XDG_RUNTIME_DIR"
  fi
  cp /etc/resolv.conf $1/etc/resolv.conf
  chroot $1 /bin/bash -l
}

stop() {
  umount -f $1/dev/pts
  umount -f $1/dev/shm
  if [ -n "$XDG_RUNTIME_DIR" ]
  then
    umount -f "$1/$XDG_RUNTIME_DIR"
  fi
  umount -f $1/proc
  umount -f $1/sys
  umount -f $1/dev
  umount -f $1/tmp
}

[ $# -lt 1 ] && exit 1

if [ "$(id -u)" -ne 0 ]
then
  exec sudo -E $0 "$@"
  exit $?
fi

case $1 in
  stop) stop $2;;
  start) start $2;;
  *) start $1;;
esac
