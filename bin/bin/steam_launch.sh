#!/bin/bash

readonly SELECTOR="${1:-fzf}"
readonly STEAM_ROOT=$HOME/.steam/steam

declare -A game_id_for_game_name

# All installed Steam games correspond with an appmanifest_<appid>.acf file
for manifest in $STEAM_ROOT/steamapps/appmanifest_*.acf; do
    name=$(awk -F\" '/"name"/ {print $4}' "$manifest")
    appid=$(basename "$manifest" | tr -dc "[0-9]")

    if [[ "$name" == *"Runtime"* ]] || [[ "$name" == "Proton"* ]]; then
        continue
    fi

    game_id_for_game_name["$name"]="$appid"
done

selected_app=$(printf "%s\n" "${!game_id_for_game_name[@]}" | $SELECTOR)
game_id=${game_id_for_game_name["$selected_app"]}

if [ -z "$game_id" ]; then
    echo "Game '$selected_app' not found."
    exit 1
fi

echo "Starting $selected_app ($game_id)..."

steam -silent steam://rungameid/$game_id &
