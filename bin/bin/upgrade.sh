#!/bin/sh

VERSION=$1
OLD_VERSION=$2
[ -z "${VERSION}" ] && exit 1

if [ -n "${OLD_VERSION}" ]
then
  sudo rm -vr /boot/*${OLD_VERSION}*
  sudo rm -vrf /lib/modules/${OLD_VERSION}*
fi

DEST=$(mktemp -d)
cd ${DEST}
tar xvf ~/linux-${VERSION}-x86.tar.xz
sudo cp -v boot/* /boot/
sudo cp -vr lib/modules/* /lib/modules/

sudo dracut -f --kver ${VERSION}
sudo update-grub

cd ${HOME}
rm -rf ${DEST}

sudo reboot
