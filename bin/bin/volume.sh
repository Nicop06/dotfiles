#!/bin/bash

# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute
# $./volume.sh switch

readonly INCREMENT=5
readonly NOTIFY_ID=666
readonly NOTIFY_TIME=2000
readonly SINK_NAME_PATTERN="analog-stereo"

readonly ICON_PREFIX="/usr/share/icons/Faba/48x48/notifications"
readonly ICON_MUTED="$ICON_PREFIX/notification-audio-volume-muted.svg"
readonly ICON_LOW="$ICON_PREFIX/notification-audio-volume-low.svg"
readonly ICON_MEDIUM="$ICON_PREFIX/notification-audio-volume-medium.svg"
readonly ICON_HIGH="$ICON_PREFIX/notification-audio-volume-high.svg"
readonly DEVICE_ICON_PREFIX="/usr/share/icons/Faba/symbolic/devices"
readonly ICON_HEADPHONE="$DEVICE_ICON_PREFIX/audio-headphones-alt-symbolic.svg"
readonly ICON_SPEAKERS="$DEVICE_ICON_PREFIX/audio-headphones-symbolic.svg"

function send_notification() {
    notify-send "$1" -i "$2" --replace-id=$NOTIFY_ID -t $NOTIFY_TIME
}

function show_volume {
    local volume=$(pamixer --get-volume)

    if [ "$volume" = "0" ]; then
        icon_name=$ICON_MUTED
    elif [ "$volume" -lt "30" ]; then
        icon_name=$ICON_LOW
    elif [ "$volume" -lt "70" ]; then
        icon_name=$ICON_MEDIUM
    else
        icon_name=$ICON_HIGH
    fi

    bar=$(printf "─%.0s" $(seq $(($volume/5))))
    send_notification "$volume""   ""$bar" "$icon_name"
}

function show_muted() {
    send_notification "Mute" $ICON_MUTED
}

function switch_sink() {
    local device_name
    local icon
    local default_sink=$(pactl get-default-sink)
    local new_sink=$(pactl list sinks short | grep -v $default_sink)
    pactl set-default-sink $(echo $new_sink | awk '{print $1;}')
    echo "$new_sink"
    if [[ "$new_sink" == *hdmi-stereo* ]]; then
        send_notification "Headphone" $ICON_HEADPHONE
    else
        send_notification "Speakers" $ICON_SPEAKERS
    fi
}

case $1 in
    up)
        volume=$(pamixer --get-volume)
        if [ "$volume" -lt "100" ]; then
            pamixer -i $INCREMENT
        fi
        show_volume
        ;;
    down)
        pamixer -d $INCREMENT
        show_volume
        ;;
    switch)
        switch_sink
        ;;
    mute)
        pamixer --toggle-mute
        if $(pamixer --get-mute); then
            show_muted
        else
            show_volume
        fi
        ;;
esac
