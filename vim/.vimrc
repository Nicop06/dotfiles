set nocompatible
filetype plugin indent on

" Set tabs size and use spaces
set softtabstop=2
set shiftwidth=2
set expandtab

" Line numbers, search and command options
set showcmd
set number
set relativenumber
set showmatch
set hlsearch
set incsearch
set ignorecase
set smartcase
set smarttab
set ruler
set scrolloff=3
set nomodeline

" Insert mode options
set backspace=2
set autoindent
set formatoptions=cqrt

" Status line
set laststatus=2

" Enable folding
set foldmethod=marker
set foldlevel=10

" Force vertical split
set diffopt+=vertical

" Clipboard
set clipboard=unnamed

" Allow switching buffers without saving
set hidden

" Enable syntax and set background
set background=dark
syntax on

" Change gvim font
set guifont=Monospace\ 18

" Change leader to comma
let mapleader = ","
let maplocalleader = ";"

" Use rg for grep
if executable('rg')
  set grepprg=rg\ --vimgrep
endif

" Clear search register
nnoremap <leader>/ :let @/=""<CR>

" Set shortcuts
map <F5> :setlocal spell! spelllang=en_us<CR>:syntax spell toplevel<CR>
nmap <F8> :TagbarToggle<CR>
nmap <F9> :NERDTreeToggle<CR>
set pastetoggle=<F10>

" Colorscheme
let g:solarized_termcolors=256
let g:solarized_termtrans=1
try
  colorscheme solarized
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme slate
endtry

" Move on the displayed lines, not real lines
noremap <silent> k gk
noremap <silent> j gj

" Bufexplorer preferences
let g:bufExplorerDisableDefaultKeyMapping=0
nnoremap <silent> <leader>bE :BufExplorer<CR>
nnoremap <silent> <leader>bT :ToggleBufExplorer<CR>
nnoremap <silent> <leader>bh :BufExplorerHorizontalSplit<CR>
nnoremap <silent> <leader>bv :BufExplorerVerticalSplit<CR>

" FZF commands
nnoremap <C-P><C-P> :Files<CR>
nnoremap <C-P>f :Files<CR>
nnoremap <C-P>b :Buffers<CR>
nnoremap <C-P>h :History<CR>
nnoremap <C-P>c :Commands<CR>

" Customize Emmet keymappings
let g:user_emmet_leader_key='<C-s>'

" UltiSnips config
let g:UltiSnipsExpandTrigger="<c-c>"
let g:UltiSnipsJumpForwardTrigger="<c-c>"
let g:UltiSnipsJumpBackwardTrigger="<c-v>"

" Vim-airline
let g:airline_powerline_fonts = 1

" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_key_detailed_diagnostics = ''
"let g:ycm_server_python_interpreter = '/usr/bin/python3'
"nnoremap <leader>jd :YcmCompleter GoTo<CR>

" CoC
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> <leader>jd <Plug>(coc-definition)
nmap <silent> <leader>jy <Plug>(coc-type-definition)
nmap <silent> <leader>ji <Plug>(coc-implementation)
nmap <silent> <leader>jr <Plug>(coc-references)
nmap <silent> <leader>js <Plug>(coc-action-workspaceSymbols)
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>ac <Plug>(coc-codeaction)
nmap <leader>ah <Plug>(coc-action-doHover)
nmap <leader>qf  <Plug>(coc-fix-current)
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
inoremap <silent><expr> <c-space> coc#refresh()

command! -nargs=0 Format :call CocActionAsync('format')

" LatexBox
let g:LatexBox_viewer = "zathura"

" Run Makefile
nnoremap <leader>r :make! run<CR>
nnoremap <leader>t :make! test<CR>

" Improve tabs management
nnoremap <leader>te :tabe<CR>
nnoremap <leader>tc :tabclose<CR>
nnoremap <leader>to :tabonly<CR>
nnoremap <C-n>      :tabnext<CR>
nnoremap <C-h>      :tabprevious<CR>
nnoremap <C-k>      :execute 'silent! tabmove +1'<CR>
nnoremap <C-j>      :execute 'silent! tabmove -1'<CR>

" GNU Global
set csprg=gtags-cscope
set cscopequickfix=s-,c-,d-,i-,t-,e-
let g:GtagsCscope_Auto_Load = 1
let g:GtagsCscope_Auto_Map = 1
let g:GtagsCscope_Quiet = 1

" Haskell
let g:haddock_browser = "/usr/bin/firefox"

" Gnupg
let g:GPGExecutable = "gpg2 --trust-model always"
let g:GPGFilePattern = "*.gpg"

" DirDiff
let g:DirDiffEnableMappings = 1

" Syntastic
let g:syntastic_cs_checkers = ['code_checker']

" ALE
let g:ale_linters = {
    \ 'python': ['ruff', 'pyright'],
    \}
let g:ale_fixers = {
    \ 'python': ['ruff'],
    \}
nmap ]g :ALENextWrap<CR>
nmap [g :ALEPreviousWrap<CR>
nmap ]G :ALELast<CR>
nmap [G :ALEFirst<CR>

" Neomake
let g:neomake_open_list = 2

" Sonic PI
let g:sonicpi_command = 'sonic-pi-tool'
let g:sonicpi_command = 'sonic-pi-tool'
let g:sonicpi_send = 'eval-stdin'
let g:sonicpi_stop = 'stop'
let g:vim_redraw = 1

" Markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']

" Reload config
nnoremap <leader>l :source ~/.vimrc<CR>

" Plugins
function PluginAdd(url)
    if a:url[0:len('http')-1] == 'http'
        let l:url = a:url
    else
        let l:url = 'https://github.com/' . a:url
    endif
    let l:name = split(l:url, '/')[-1]
    execute '!cd ~/.vim && git submodule add -f ' . l:url . ' ./pack/plugins/start/' .  l:name
    helptags ALL
endfunction

function PluginRemove(name)
    if a:name[0:len('http')-1] == 'http'
        let l:name = split(a:name, '/')[-1]
    else
        let l:name = a:name
    endif
    let l:path = './pack/plugins/start/' . l:name
    execute '!cd ~/.vim && git submodule deinit -f ' . l:path . ' && git rm -f ' . l:path
endfunction

command! -nargs=1 PluginAdd call PluginAdd(<args>)
command! -nargs=1 PluginRemove call PluginRemove(<args>)
